#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"


const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *new_hash = md5(guess, strlen(guess));
    // Compare the two hashes
    //fprintf(stderr, "%s %s\n", new_hash, hash);
    if(strcmp(hash, new_hash) == 0)
    {
        return 1;
    }
    // Free any malloc'd memory
    free(new_hash);
    return 0;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    FILE *f = fopen(filename,"r");
    char line[PASS_LEN];
    *size = 50;
    int count = 0;
    char **words = malloc(*size * sizeof(char *));
    while(fgets(line, PASS_LEN, f ) != NULL)
    {
        if( count == *size )
        {
            *size += 50;
            words = realloc(words, *size * (sizeof(char *)));
        }
        line[strlen(line)-1] = '\0';
        char *word = malloc(strlen(line) * sizeof(char) + 1);
        strcpy(word,line);
        words[count] = word;
        count++;
    }
    /*
    for(int i = 0; i < *size; i++)
    {
        fprintf(stderr, "%d %s\n", i, words[i]);
    }
    */
    fclose(f);
    return words;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    FILE *hash = fopen( argv[1], "r" );
    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);
    
    // Open the hash file for reading.
    char line[100];
    while(fgets(line, 1000, hash) != NULL)
    {
        line[strlen(line)-1] = '\0';
        for(int x = 0; x < dlen; x++)
        {
            if(tryguess(line,dict[x]) == 1)
            {
                fprintf(stderr,"Match: %s %s\n", line, dict[x]);
                break;
            }
        }
    }
    free(dict);
    fclose(hash);
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
}
